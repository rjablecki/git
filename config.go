package git

type Config struct {
	Host       string
	Port       string
	User       string
	Password   string
	PrivateKey string
}

func NewConfig(host, port, user, password, keyName string) *Config {
	c := new(Config)
	c.Host = host
	c.Port = port
	c.User = user
	c.Password = password
	c.PrivateKey = keyName

	return c
}
