package git

import (
	"regexp"
	"strconv"
	"strings"
	"time"
)

func (r *Repository) ReadBranches() error {
	var branches []*Branch
	res, err := r.Execute("git branch -va")

	if err != nil {
		return err
	}

	lines := strings.Split(res, "\n")

	for _, oneLine := range lines {
		trimmed := strings.TrimSpace(oneLine)
		if len(trimmed) == 0 {
			continue
		}
		aaa := regexp.MustCompile(`\s*(\*)?\s*(\S*)\s*(\S*)\s*(.*)$`)
		result := aaa.FindAllStringSubmatch(oneLine, -1)

		branch := new(Branch)
		branch.IsActive = result[0][1] == "*"
		branch.Hash = result[0][3]
		branch.Commit = result[0][4]

		if strings.Index(result[0][2], "remotes") == 0 {
			branch.IsRemote = true
			branch.Name = result[0][2][8:]
		} else {
			branch.IsRemote = false
			branch.Name = strings.Trim(result[0][2], " \t")
		}

		branches = append(branches, branch)
	}

	var parts []string

	for _, b := range branches {
		parts = append(parts, "git log "+b.Hash+" -n 1 --pretty=tformat:%cn###%cr###%ct###%s###"+b.Name)
	}

	commitsData, _ := r.Execute(strings.Join(parts, " && "))
	commitLines := strings.Split(commitsData, "\n")

	for _, lineNotTrimmed := range commitLines {
		line := strings.TrimSpace(lineNotTrimmed)
		if len(line) == 0 {
			continue
		}
		reg := regexp.MustCompile(`^(.*)###(.*)###(.*)###(.*)###(.*)$`)
		m := reg.FindAllStringSubmatch(line, -1)

		branchName := m[0][5]
		subject := m[0][4]
		if len(subject) > 48 {
			subject = subject[:48]
		}

		authors := strings.Split(m[0][1], " ")
		author := strings.ToLower(authors[0])

		for _, b := range branches {
			if b.Name == branchName {
				b.Subject = subject
				b.LastTime = m[0][2]
				unix, _ := strconv.ParseInt(m[0][3], 10, 64)
				b.Timestamp = time.Unix(unix, 0)
				b.Author = author
			}
		}
	}

	r.branches = branches

	return nil
}

func (r *Repository) ReadLogs() error {

	res, err := r.Execute("git log -n 50 --pretty=format:%h_%ct_%cn_%s")
	if err != nil {
		return err
	}

	var logs []*Log
	lines := strings.Split(res, "\n")
	for _, line := range lines {
		log := new(Log)
		line = strings.TrimSpace(line)
		if len(line) == 0 {
			continue
		}
		pattern := regexp.MustCompile(`^([^_]+)_([0-9]+)_([^_]+)_(.+)$`)
		matches := pattern.FindAllStringSubmatch(line, -1)

		timestamp, err := strconv.Atoi(matches[0][2])
		if err != nil {
			timestamp = 0
		}

		authors := strings.Split(matches[0][3], " ")
		author := strings.ToLower(authors[0])

		subject := matches[0][4]

		log.Subject = subject
		log.Author = author
		log.Hash = matches[0][1]
		log.Timestamp = time.Unix(int64(timestamp), 0)

		logs = append(logs, log)
	}

	r.logs = logs

	return nil
}

func (r *Repository) ReadStatus() error {

	result, err := r.Execute("git status --porcelain")
	if err != nil {
		return err
	}

	status := new(Status)

	status.M = []string{}
	status.Mu = []string{}
	status.N = []string{}
	status.Nu = []string{}
	status.D = []string{}
	status.Du = []string{}
	status.A = []string{}

	if len(result) == 0 {
		status.Ready = false
		status.Clear = true
	} else {
		status.Clear = false
		lines := strings.Split(result, "\n")

		for _, line := range lines {
			trimmed := strings.TrimSpace(line)
			if len(trimmed) == 0 {
				continue
			}
			prefix := line[:2]
			file := line[2:]
			file = strings.TrimSpace(file)
			switch prefix {
			case "??":
				status.Nu = append(status.Nu, file)
				break
			case "AM":
				status.Nu = append(status.Nu, file)
				break
			case "M ":
				status.M = append(status.M, file)
				break
			case "MM":
				status.Mu = append(status.Mu, file)
				break
			case " M":
				status.Mu = append(status.Mu, file)
				break
			case "D ":
				status.D = append(status.D, file)
				break
			case " D":
				status.Du = append(status.Du, file)
				break
			case "A ":
				status.A = append(status.A, file)
				break
			default:

			}
		}

		if len(status.Nu) > 0 || len(status.Mu) > 0 || len(status.Du) > 0 {
			status.Ready = false
		} else {
			status.Ready = true
		}
	}

	r.status = status

	return nil
}
