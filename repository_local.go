package git

type LocalRepository struct {
	Repository
}

func NewLocalRepository(path string) *LocalRepository {
	rep, _ := NewRepository(SourceLocal, path)
	r := &LocalRepository{Repository: *rep}
	r.readRemote()
	r.connected = true

	return r
}
