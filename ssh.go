package git

import (
	"fmt"
	"golang.org/x/crypto/ssh"
	"io/ioutil"
	"os/user"
	"time"
)

var (
	sessions map[string]*ssh.Client
)

func makeConnection(config *Config) (*ssh.Session, *ssh.Client, error) {
	var err error

	if sessions == nil {
		sessions = make(map[string]*ssh.Client)
	}

	address := fmt.Sprintf("%s:%s", config.Host, config.Port)

	if _, ok := sessions[address]; ok == false {
		configSSH := &ssh.ClientConfig{
			User:    config.User,
			Timeout: 5 * time.Second,
		}
		if config.Password != "" {
			configSSH.Auth = []ssh.AuthMethod{ssh.Password(config.Password)}
		} else if config.PrivateKey != "" {
			publicKey, err := publicKeyFile(getPrivateKeyPath(config))

			if err != nil {
				return nil, nil, err
			}
			configSSH.Auth = []ssh.AuthMethod{publicKey}
		}
		ca, err := ssh.Dial("tcp", address, configSSH)
		if err != nil {
			return nil, nil, err
		}

		sessions[address] = ca
	}

	client := sessions[address]
	session, err := client.NewSession()

	if err != nil {
		return nil, nil, err
	}

	return session, client, err
}

func getPrivateKeyPath(config *Config) string {
	me, _ := user.Current()
	return fmt.Sprintf("%s/.ssh/%s", me.HomeDir, config.PrivateKey)
}

func publicKeyFile(file string) (ssh.AuthMethod, error) {
	buffer, err := ioutil.ReadFile(file)
	if err != nil {
		return nil, err
	}
	key, err := ssh.ParsePrivateKey(buffer)
	if err != nil {
		return nil, err
	}
	return ssh.PublicKeys(key), nil
}
