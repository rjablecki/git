package git

import (
	"errors"
	"fmt"
	"log"
	"os"
)

type ModuleManager struct {
	modules []*Module
}

func NewModuleManger(dir string) *ModuleManager {

	m := new(ModuleManager)

	log.Print(fmt.Sprintf("NewModuleManger dir: '%s'", dir))

	for _, name := range m.checkModules(dir) {
		log.Printf("Initialize module: '%s'", name)

		localPath := fmt.Sprintf("%s\\%s", dir, name)
		module := NewModule(localPath, name)

		m.AddModule(module)
	}

	return m
}

func (m *ModuleManager) Modules() []*Module {
	return m.modules
}

func (m *ModuleManager) AddModule(module ...*Module) *ModuleManager {

	m.modules = append(m.modules, module...)

	return m
}

func (m *ModuleManager) GetModule(name string) (*Module, error) {

	for _, module := range m.modules {
		if module.GetName() == name {
			return module, nil
		}
	}

	return nil, errors.New(fmt.Sprintf("Module '%s' not exists", name))
}

func (m *ModuleManager) checkModules(dir string) []string {

	var modules []string

	da, err := os.Open(dir)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	defer da.Close()
	fi, err := da.Readdir(-1)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	for _, fi := range fi {
		if fi.IsDir() {
			fi.Name()
			d, err := os.Open(fmt.Sprintf("%s/%s/.git", dir, fi.Name()))
			if err == nil {
				d.Close()
				modules = append(modules, fi.Name())
			}
		}
	}

	return modules
}
