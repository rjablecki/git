package git

type Status struct {
	M     []string
	Mu    []string
	N     []string
	Nu    []string
	D     []string
	Du    []string
	A     []string
	Clear bool
	Ready bool
}
