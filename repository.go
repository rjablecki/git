package git

import (
	tl "bitbucket.org/anthracis/tools"
	"fmt"
)

const (
	SourceLocal  = "local"
	SourceRemote = "remote"

	RepoStatusClear = "clear"
	RepoStatusDirty = "dirty"
)

type Repository struct {
	source    string
	path      string
	status    *Status
	branches  []*Branch
	logs      []*Log
	Remote    *RemoteConfig
	ConfigSSH *RemoteConfig
	connected bool
}

func NewRepository(source, path string) (*Repository, error) {
	r := new(Repository)
	r.source = source
	r.path = path
	r.status = new(Status)

	return r, nil
}

func (r *Repository) checkConnection() *Repository {
	if r.source == SourceLocal {
		r.connected = true
	} else {
		_, err := remoteCmd("ls -l", "", &r.ConfigSSH.Config)
		r.connected = err == nil
	}
	return r
}

func (r *Repository) IsConnected() bool {
	return r.connected
}

func (r *Repository) IsLocal() bool {
	return r.source == SourceLocal
}

func (r *Repository) readRemote() {
	var remoteName, remoteUrl string

	res, _ := r.Execute("git remote")
	lines := tl.GetLines(res)
	if lines != nil {
		remoteName = lines[0]
	}

	if len(remoteName) > 0 {
		res, _ := r.Execute(fmt.Sprintf("git config --get remote.%s.url", remoteName))
		lines := tl.GetLines(res)
		if lines != nil {
			remoteUrl = lines[0]
		}
	}

	r.Remote = NewRemote(remoteName, remoteUrl)
	fillRemote(r.Remote)
}

func (r *Repository) UpdateInfo() error {
	var err error

	go r.fetch()

	err = r.ReadStatus()
	if err != nil {
		return err
	}
	err = r.ReadBranches()
	if err != nil {
		return err
	}
	err = r.ReadLogs()

	return err
}

func (r *Repository) Execute(cmd string) (string, error) {
	if r.source == SourceLocal {
		return localCmd("cmd /C "+cmd, r.path)
	}
	return remoteCmd(cmd, r.path, &r.ConfigSSH.Config)
}

func (r *Repository) GetBranches() []*Branch {
	return r.branches
}

func (r *Repository) GetLogs() []*Log {
	return r.logs
}

func (r *Repository) GetStatus() *Status {
	return r.status
}

func (r *Repository) GetCurrentBranch() *Branch {
	for _, branch := range r.branches {
		if branch.IsActive {
			return branch
		}
	}
	return nil
}

func (r *Repository) stash() (string, error) {
	return r.Execute("git stash")
}

func (r *Repository) addAll() (string, error) {
	return r.Execute("git add .")
}

func (r *Repository) commit(message string) (string, error) {
	return r.Execute(fmt.Sprintf("git commit -m '%s'", message))
}

func (r *Repository) fetch() (string, error) {
	return r.Execute(fmt.Sprintf("git fetch -p %s", r.Remote.Name))
}

func (r *Repository) createBranch(name string, source string) (string, error) {
	return r.Execute(fmt.Sprintf("git branch %s %s", name, source))
}

func (r *Repository) removeBranch(name string) (string, error) {
	return r.Execute(fmt.Sprintf("git branch -D %s", name))
}

func (r *Repository) checkout(branchName string) (string, error) {
	return r.Execute(fmt.Sprintf("git checkout %s", branchName))
}

func (r *Repository) checkoutB(name string, source string) (string, error) {
	return r.Execute(fmt.Sprintf("git checkout -b %s %s", name, source))
}

func (r *Repository) push(branchName string) (string, error) {
	return r.Execute(fmt.Sprintf("git push %s %s", r.Remote.Name, branchName))
}
