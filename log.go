package git

import "time"

type Log struct {
	Timestamp time.Time
	Hash      string
	Author    string
	Subject   string
}
