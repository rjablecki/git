package git

import (
	"bitbucket.org/anthracis/tools"
	"fmt"
	"github.com/mikkeloscar/sshconfig"
	"log"
	"os/user"
	"strings"
)

var hosts map[string]*sshconfig.SSHHost

func readConfigFile() {
	me, err := user.Current()

	if err != nil {
		log.Print("Error: " + err.Error())
		return
	}

	hosts = make(map[string]*sshconfig.SSHHost)
	readHosts, err := sshconfig.ParseSSHConfig(fmt.Sprintf("%s\\.ssh\\config", me.HomeDir))

	if err != nil {
		log.Print("readConfigFile")
		log.Print(tools.VarDump(err))
	} else {
		for _, host := range readHosts {
			for _, name := range host.Host {
				log.Print("name", name)
				hosts[name] = host
			}
		}
	}
}

func fillRemote(remote *RemoteConfig) bool {

	res := false
	if hosts == nil || len(hosts) == 0 {
		readConfigFile()
	}

	if conf, ok := hosts[remote.Host]; ok {
		remote.Port = fmt.Sprintf("%v", conf.Port)
		remote.User = conf.User
		remote.Host = conf.HostName
		remote.PrivateKey = strings.Replace(conf.IdentityFile, `~/.ssh/`, "", -1)
		res = true
	}

	return res
}
