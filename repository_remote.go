package git

type RemoteRepository struct {
	Repository
}

func NewRemoteRepository(gr *RemoteConfig) *RemoteRepository {
	rep, _ := NewRepository(SourceRemote, gr.Path)
	rep.ConfigSSH = gr
	r := &RemoteRepository{Repository: *rep}

	r.readRemote()

	go r.ReadBranches()
	go r.ReadStatus()
	go r.ReadLogs()

	return r
}
