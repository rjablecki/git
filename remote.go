package git

import (
	"log"
	"regexp"
	"strings"
)

type RemoteConfig struct {
	Url  string
	Name string
	Path string
	Config
}

func NewRemote(name, url string) *RemoteConfig {

	r := new(RemoteConfig)
	r.Name = name
	r.Url = url

	url = strings.Replace(url, "ssh://", "", -1)
	url = strings.Replace(url, "https://", "", -1)
	url = strings.Replace(url, "http://", "", -1)
	url = strings.Replace(url, ".git", "", -1)
	url = strings.TrimRight(url, "/")

	log.Print("git remote name: " + name)
	log.Print("git remote url: " + url)

	pattern := regexp.MustCompile(`^([^@]+)@([^/:]+)(.*)$`)
	matches := pattern.FindAllStringSubmatch(url, -1)

	if matches != nil && len(matches) > 0 {
		m := matches[0]
		r.User = m[1]
		r.Host = m[2]
		r.Path = m[3]
	} else {
		pattern = regexp.MustCompile(`^([^/:]+)(/.*)$`)
		matches = pattern.FindAllStringSubmatch(url, -1)

		if matches != nil && len(matches) > 0 {
			m := matches[0]
			r.Host = m[1]
			r.Path = m[2]
		}
	}

	return r
}
