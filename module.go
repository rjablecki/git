package git

import (
	tl "bitbucket.org/anthracis/tools"
	"fmt"
	"log"
	"sync"
	"time"
)

type Module struct {
	name   string
	Local  *LocalRepository
	Remote *RemoteRepository
}

func NewModule(path, name string) *Module {
	module := new(Module)
	module.name = name
	module.Local = NewLocalRepository(path)

	go module.Local.UpdateInfo()

	module.Remote = NewRemoteRepository(module.Local.Remote)
	module.Remote.checkConnection()

	go func() {
		err := module.Remote.UpdateInfo()
		if err != nil {
			log.Print(fmt.Sprintf("R[%s] %s", tl.Color(name, tl.YELLOW), tl.Color(err.Error(), tl.RED)))
		}
	}()

	return module
}

func (m *Module) GetName() string {
	return m.name
}

func (m *Module) UpdateInfo() *Module {
	var wg sync.WaitGroup
	wg.Add(2)
	go func() {
		defer wg.Done()
		m.Local.UpdateInfo()
	}()
	go func() {
		defer wg.Done()
		m.Remote.UpdateInfo()
	}()
	wg.Wait()

	return m
}

func (m *Module) Fetch() string {
	out, _ := m.Remote.fetch()

	defer m.UpdateInfo()

	return out
}

func (m *Module) Push(branchName string) string {
	out, _ := m.Remote.push(branchName)

	defer m.UpdateInfo()

	return out
}

func (m *Module) CreateBranch(branchName string, sourceBranch string) string {
	var out string

	if !m.Remote.GetStatus().Clear {
		out += "\n Masz nieskommitowane zmiany"
		return out
	}

	checkoutResponse, _ := m.Remote.checkoutB(branchName, sourceBranch)
	m.Remote.UpdateInfo()

	out += checkoutResponse
	out += m.Refresh()

	defer m.UpdateInfo()

	return out
}

func (m *Module) Checkout(branchName string) string {
	var out string

	if !m.Remote.GetStatus().Clear {
		out += "\n Masz nieskommitowane zmiany"
		return out
	}

	//@todo sprawdzenie czy nie checkout na tą samą

	checkoutRemoteResponse, _ := m.Remote.checkout(branchName)
	out += checkoutRemoteResponse
	m.Remote.UpdateInfo()

	remoteBranch := m.Remote.GetCurrentBranch()

	fetchResponse, _ := m.Local.fetch()
	out += fetchResponse

	addResponse, _ := m.Local.addAll()
	out += addResponse

	stashResponse, _ := m.Local.stash()
	out += stashResponse

	tmpName := fmt.Sprintf("t%v", time.Now().Unix())

	checkoutResponse, _ := m.Local.checkoutB(tmpName, "")
	out += checkoutResponse

	removeResponse, _ := m.Local.removeBranch(remoteBranch.Name)
	out += removeResponse

	createBranchResponse, _ := m.Local.checkoutB(remoteBranch.Name, fmt.Sprintf("%s/%s", m.Local.Remote.Name, remoteBranch.Name))
	out += createBranchResponse

	removeResponse, _ = m.Local.removeBranch(tmpName)
	out += removeResponse

	defer m.UpdateInfo()

	return out
}

func (m *Module) Refresh() string {
	var out string

	if !m.Remote.GetStatus().Clear {
		out += "\n Masz nieskommitowane zmiany"
		return out
	}

	remoteBranch := m.Remote.GetCurrentBranch()

	fetchResponse, _ := m.Local.fetch()
	out += fetchResponse

	addResponse, _ := m.Local.addAll()
	out += addResponse

	stashResponse, _ := m.Local.stash()
	out += stashResponse

	tmpName := fmt.Sprintf("t%v", time.Now().Unix())

	checkoutResponse, _ := m.Local.checkoutB(tmpName, "")
	out += checkoutResponse

	removeResponse, _ := m.Local.removeBranch(remoteBranch.Name)
	out += removeResponse

	createBranchResponse, _ := m.Local.checkoutB(remoteBranch.Name, fmt.Sprintf("%s/%s", m.Local.Remote.Name, remoteBranch.Name))
	out += createBranchResponse

	removeResponse, _ = m.Local.removeBranch(tmpName)
	out += removeResponse

	defer m.UpdateInfo()

	return out
}
