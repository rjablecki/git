package git

import "time"

type Branch struct {
	Name      string
	Author    string
	Hash      string
	Commit    string
	Subject   string
	LastTime  string
	Timestamp time.Time
	IsRemote  bool
	IsActive  bool
}
