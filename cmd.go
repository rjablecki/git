package git

import (
	"bytes"
	"fmt"
	"log"
	"os/exec"
	"strings"
)

func nameByPath(dir string) string {
	parts := strings.Split(dir, "\\")
	last := parts[len(parts)-1]
	parts = strings.Split(last, "/")

	return parts[len(parts)-1]
}

func localCmd(cmd string, dir string) (string, error) {
	var out bytes.Buffer
	var stderr bytes.Buffer

	cmda := strings.Replace(cmd, "cmd /C ", "", -1)
	if len(cmda) > 70 {
		cmda = cmda[:70]
	}

	log.Print(fmt.Sprintf("L[%s] %s", nameByPath(dir), cmda))

	cmdArgs := strings.Split(cmd, " ")
	command := exec.Command("cmd", cmdArgs...)
	command.Dir = dir
	command.Stdout = &out
	command.Stderr = &stderr
	err := command.Run()

	if err != nil {
		return "", err
	}

	return out.String(), nil
}

func remoteCmd(cmd string, dir string, config *Config) (string, error) {
	session, _, err := makeConnection(config)
	if err != nil {
		return "", err
	}
	//	defer client.Close()
	defer session.Close()
	var b bytes.Buffer
	session.Stdout = &b

	cmda := cmd
	if len(cmda) > 70 {
		cmda = cmda[:70]
	}
	log.Print(fmt.Sprintf("R[%s] %s", nameByPath(dir), cmda))

	err = session.Run(fmt.Sprintf("cd %s && %s", dir, cmd))
	if err != nil {
		return "", err
	}

	return b.String(), nil
}
